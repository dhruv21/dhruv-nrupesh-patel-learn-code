import socket

def server_atm_code():
    host = socket.gethostname()
    port_number = 8008 # initiate port no above 1024
    pin_number = 9999
    max_attempts = 3
    balance = 10000
    OPTIONS = 0
    OPTIONS_MESSAGE = ["Enter your PIN",
                     "Enter amount to withdraw",
                     "Insufficient balance" ]
    socket_server = socket.socket() 
    socket_server.bind((host, port_number))  
    socket_server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    socket_server.listen(2)
    conn, address = socket_server.accept()  
    print("Connection from: " + str(address))
    conn.send(OPTIONS_MESSAGE[OPTIONS].encode())

    while True:
        data = conn.recv(1024).decode()
        if not data:
            break
        print("From connected user: " + str(data))
        
        try:
            if ( OPTIONS == 0 ) and ( int(data) == pin_number ):
                OPTIONS = OPTIONS + 1
                print("Valid Pin")
                data = OPTIONS_MESSAGE[OPTIONS]
                conn.send(data.encode())
            elif( OPTIONS == 0 ) and ( int(data) != pin_number ):
                max_attempts = max_attempts - 1
                if (max_attempts == 0):
                    raise Exception("Too many invalid attempts. Your account is locked. Contact your bank for futher information.")
                raise Exception("Invalid Pin. You have only" + str(max_attempts) + " chances left.")
            elif ( OPTIONS == 1 ) and ( int(data) <= balance ):
                OPTIONS = OPTIONS + 1
                data = "Amount withdrawal successful. Transaction completed."
                print(data)
                conn.send(data.encode())   
                conn.close()   
                break
            elif( OPTIONS == 1 ) and ( int(data) > balance ):
                raise Exception("Insufficient balance.")
            else:
                print(data)

        except Exception as e:
            error = e.args
            print(error)
            if error[0] == "Too many invalid attempts. Your account is locked. Contact your bank for futher information.":
                conn.send(error[0].encode())
                conn.close() 
                break
            else:
                error_message = error[0] + " Enter again."
                conn.send(error_message.encode())

    conn.close()  
    server_socket.close()

if __name__ == '__main__':
    server_atm_code()