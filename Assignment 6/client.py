import socket
import sys

def client_atm_code():
    host = socket.gethostname()  
    port_number = 8008  
    isconnected = False
    socket_client = socket.socket()  
    socket_client.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    
    try:
    	socket_client.connect((host, port_number)) 
    	isconnected = True
    except Exception as e:
    	print(e)
    	print("Cannot connect to server. Try again after some time.")
    while isconnected:
        data = socket_client.recv(1024).decode()  
        if data == "Amount withdrawal successful. Transaction completed.":
        	print(data)
        	socket_client.close() 
        	break;
        elif data == "Too many invalid attempts. Your account is locked. Contact your bank for futher information.":
        	print(data)
        	socket_client.close() 
        	break;        	
        print('Received from server: ' + data)  
        message = input(" -> ")  
        socket_client.send(message.encode())
    socket_client.close()  

def main():
	client_atm_code()

if __name__=="__main__":
    main()