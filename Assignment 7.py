import requests 
from requests.exceptions import HTTPError

def wrapper(item):
  def wrapped_function():
    location = input('Enter the location: ')
    try:
      api_request = requests.get('https://api.opencagedata.com/geocode/v1/json?q='+location +'&key=06bacb63834046b7a7c700c64725dd3f')
      api_response = api_request.json()
      api_result = (api_response['results'][0]['geometry'])
      return 'The latitude and longitude of the entered location is {}' .format(str(api_result))

    except HTTPError as http_err:
      print(f'HTTP error occurred: {http_err}')
    except Exception as err:
      print(f'Other error occurred: {err}')
  return wrapped_function

@wrapper
def print_results():
   return ' '

print(print_results())